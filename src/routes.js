const { Router } = require('express');

const Perfil = require('Perfil');


const routes = new Router();

routes.post('/perfil', Perfil.store);
routes.put('/perfil/:idPerfil', Perfil.update);
routes.delete('/perfil/:idPerfil', Perfil.delete);
routes.get('/perfil', Perfil.index);

module.exports = routes;