const { Client } = require('pg');
const dbConfig = require('package').dbConfig;

class Perfil {
    async store(req, resp) {
        try {
            const { perfil, isadmin } = req.body;
            const db = new Client(dbConfig);
            await db.connect();
            const sqlInsert =   "INSERT INTO tbperfil " +
                "VALUES (DEFAULT, $1, $2)";
            const users  = await db.query(sqlInsert, [perfil, isadmin]);

            return resp.json({ info: "Perfil cadastrado com sucesso"});
        } catch (err) {
            return resp.status(400).json({ erro: "Não foi possível cadastrar o perfil"});
        }

    }

    async update(req, resp) {
        if (isNaN(req.params.idPerfil) || req.params.idPerfil < 1) {
            return resp.status(400).json({ erro: "O idPerfil não é válido"});
        }
        try {
            const { perfil, isadmin } = req.body;
            const db = new Client(dbConfig);
            await db.connect();
            const sqlSelect =   "SELECT perfil " +
                "FROM tbperfil " +
                "WHERE idperfil = " + req.params.idPerfil;
            const user = await db.query(sqlSelect);

            if (perfil && user.rows[0].perfil === perfil) {
                return resp.status(401).json({ error: 'Esse perfil já existe'});
            }

            if (perfil === undefined && isadmin !== undefined) {
                const sqlUpdate =   "UPDATE tbperfil " +
                    "SET isadmin = $1 " +
                    "WHERE idperfil = " + req.params.idPerfil;

                await db.query(sqlUpdate, [isadmin]);
                return resp.status(200).json({ info: "Perfil alterado com sucesso"})
            }

            const sqlUpdate =   "UPDATE tbperfil " +
                "SET perfil = $1, " +
                "isadmin = $2 " +
                "WHERE idperfil = " + req.params.idPerfil;
            await db.query(sqlUpdate, [perfil,isadmin]);

            return resp.json({ message: "Perfil alterado com sucesso"});
        } catch (err) {
            console.log(err);
            return resp.status(400).json({ erro: "Não foi possível atualizar o perfil"});
        }


    }

    async delete(req, resp) {
        if (isNaN(req.params.idPerfil) || req.params.idPerfil < 1) {
            return resp.status(400).json({ erro: "O idPerfil não é válido"});
        }
        try{
            const db = new Client(dbConfig);
            await db.connect();
            const sqlDelete = "DELETE FROM tbperfil WHERE idperfil = " + req.params.idPerfil;
            await db.query(sqlDelete);
            return resp.json({ message: "Perfil deletado com sucesso" });
        } catch (err) {
            return resp.status(400).json({ erro: "Não foi possível deletar o perfil"});
        }

    }

    async index(req, resp) {
        const db = new Client(dbConfig);
        await db.connect();
        const sqlSelect = "SELECT * FROM tbperfil";
        const perfil = await db.query(sqlSelect);
        return resp.json(perfil.rows);
    }
}
module.exports = new Perfil();